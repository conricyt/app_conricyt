﻿using appconricyt.Utilities;
namespace appconricyt.Core
{
    public static class ExecutionParameters
    {
        private static string _authToken;

        public static string AuthToken
        {
            get
            {
                if (_authToken != null)
                    return _authToken;
                else
                    return null;
            }
            set
            {
                _authToken = value;
            }
        }

        private static string _summonApiKey;

        public static string SummonApiKey
        {
            get
            {
                if (_summonApiKey != null)
                {
                    return _summonApiKey;
                }
                else if (Settings.SummonApiKey != null)
                {
                    _summonApiKey = Settings.SummonApiKey;
                    return _summonApiKey;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                _summonApiKey = value;
                if (KeepMeLoggedIn)
                    Settings.SummonApiKey = value;
            }
        }


        private static string _summonApiUser;

        public static string SummonApiUser
        {
            get
            {
                if (_summonApiUser != null)
                {
                    return _summonApiUser;
                }
                else if (Settings.SummonApiUser != null)
                {
                    _summonApiUser = Settings.SummonApiUser;
                    return _summonApiUser;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                _summonApiUser = value;
                if (KeepMeLoggedIn)
                    Settings.SummonApiUser = value;
            }
        }


        private static string _summonApiInstance;

        public static string SummonApiInstance
        {
            get
            {
                if (_summonApiInstance != null)
                {
                    return _summonApiUser;
                }
                else if (Settings.SummonApiInstance != null)
                {
                    _summonApiInstance = Settings.SummonApiInstance;
                    return _summonApiInstance;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                _summonApiInstance = value;
                if (KeepMeLoggedIn)
                    Settings.SummonApiInstance = value;
            }
        }


        private static bool _isLogged = false;

        public static bool IsLogged
        {
            get
            {
                if(Settings.IsLogged)
                {
                    _isLogged = Settings.IsLogged;
                    return Settings.IsLogged;
                }

                return _isLogged;
            }
            set
            {
                _isLogged = value;
                if (KeepMeLoggedIn)
                    Settings.IsLogged = value;
            }
        }

        public static bool KeepMeLoggedIn { get; set; }        

        private static string _userName;

        public static string UserName
        {
            get
            {
                if (_userName != null)
                {
                    return _userName;
                }                    
                else if (Settings.Username != null)
                {
                    _userName = Settings.Username;
                    return _userName;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                _userName = value;
                if(KeepMeLoggedIn)
                    Settings.Username = value;
            }
        }

        private static string _password;

        public static string Password
        {
            get
            {
                if (_password != null)
                {
                    return _password;
                }
                else if(Settings.Password != null)
                {
                    _password = Settings.Password;
                    return _password;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                _password = value;
                if (KeepMeLoggedIn)
                    Settings.Password = value;
            }
        }

        #region Summon Search Parameters
        private static bool _filterAuthor = false;
        public static bool FilterAuthor
        {
            get
            {
                return _filterAuthor;
            }
            set
            {
                _filterAuthor = value;
            }
        }

        private static bool _filterFullText = false;
        public static bool FilterFullText
        {
            get
            {
                return _filterFullText;
            }
            set
            {
                _filterFullText = value;
            }
        }

        private static bool _filterBook = false;
        public static bool FilterBook
        {
            get
            {
                return _filterBook;
            }
            set
            {
                _filterBook = value;
            }
        }

        private static bool _filterJournalArticle = false;
        public static bool FilterJournalArticle
        {
            get
            {
                return _filterJournalArticle;
            }
            set
            {
                _filterJournalArticle = value;
            }
        }
        #endregion
    }
}