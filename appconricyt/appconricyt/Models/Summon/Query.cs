﻿using System.Collections.Generic;

namespace appconricyt.Models.Summon
{
    public class Query
    {
        public string QueryString { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public List<TextQueries> TextQueries { get; set; }
    }
}