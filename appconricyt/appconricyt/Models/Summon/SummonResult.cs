﻿using System.Collections.Generic;

namespace appconricyt.Models.Summon
{
    public class SummonResult
    {
        public string SessionId { get; set; }

        public int ElapsedQueryTime { get; set; }

        public int QueryTime { get; set; }

        public int TotalRequestTime { get; set; }

        public int PageCount { get; set; }

        public int RecordCount { get; set; }

        public Query Query { get; set; }

        public List<Document> Documents { get; set; }
    }
}
