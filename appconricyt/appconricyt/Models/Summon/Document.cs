﻿using System.Collections.Generic;

namespace appconricyt.Models.Summon
{
    public class Document
    {
        public bool HasFullText { get; set; }

        public bool IsFullTextHit { get; set; }

        public bool IsPrint { get; set; }

        public bool InHoldings { get; set; }

        public string OpenUrl { get; set; }

        public string Link { get; set; }

        public List<string> PublicationSeriesTitle { get; set; }

        public List<string> URI { get; set; }

        public List<string> Publisher { get; set; }

        public List<string> AbstractList { get; set; }

        public List<string> Language { get; set; }

        public List<string> Snippet { get; set; }

        public List<string> Abstract { get; set; }

        public List<string> PublicationYear { get; set; }

        public List<string> Title { get; set; }

        public List<string> Subtitle { get; set; }

        public List<string> PublicationDate { get; set; }

        public List<string> Author { get; set; }

        public List<string> ISBN { get; set; }

        public List<string> ISSN { get; set; }

        public List<string> EISBN { get; set; }

        public List<string> PageCount { get; set; }

        public List<string> LinkModel { get; set; }
    }
}
