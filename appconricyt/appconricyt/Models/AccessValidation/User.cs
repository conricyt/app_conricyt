﻿using appconricyt.Models.AccessValidation;
using Newtonsoft.Json;

namespace appconricyt.Core.Models.AccessValidation
{
    public class User
    {
        public bool Is_valid { get; set; }

        [JsonProperty(PropertyName = "user")]
        public string Username { get; set; }

        public Institution Intitution { get; set; }

        public Api Api { get; set; }
    }
}
