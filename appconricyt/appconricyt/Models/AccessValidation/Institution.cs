﻿
namespace appconricyt.Core.Models.AccessValidation
{
    public class Institution
    {
        public int Id { get; set; }

        public string Inst_name { get; set; }

        public int Group { get; set; }

        public string Shortname { get; set; }

        public int Status { get; set; }
    }
}
