﻿
using appconricyt.Models.AccessValidation;

namespace appconricyt.Core.Models.AccessValidation
{
    public class IPAddress
    {
        public bool Is_valid { get; set; }

        public Institution Intitution { get; set; }

        public Api Api { get; set; }
    }
}
