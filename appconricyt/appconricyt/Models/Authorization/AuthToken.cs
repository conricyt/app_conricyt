﻿
namespace appconricyt.Core.Models.Authorization
{
    public class AuthToken
    {
        public Client Client { get; set; }

        public string Access_token { get; set; }
    }
}
