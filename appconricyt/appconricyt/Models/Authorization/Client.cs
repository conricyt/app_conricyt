﻿using System.Collections.Generic;

namespace appconricyt.Core.Models.Authorization
{
    public class Client
    {
        public int Id { get; set; }

        public string Client_id { get; set; }

        public string Scope { get; set; }

        public List<string> Grants { get; set; }

        public List<string> RedirectUris { get; set; }
    }
}
