﻿using System.Collections.Generic;

namespace appconricyt.Core.Models.PublisherResource
{
    public class Publisher
    {
        public Publisher()
        {
            Resources = new List<Resource>();
        }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Description { get; set; }

        public string Other_text { get; set; }

        public string Img { get; set; }

        public List<Resource> Resources { get; set; }
    }
}
