﻿using System.Collections.Generic;

namespace appconricyt.Core.Models.PublisherResource
{
    public class PublishersResources
    {
        public PublishersResources()
        {
            Publishers = new List<Publisher>();
        }

        public bool Is_valid { get; set; }

        public List<Publisher> Publishers { get; set; }
    }
}
