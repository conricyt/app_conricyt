﻿namespace appconricyt.Core.Models.PublisherResource
{
    public class Resource
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Description { get; set; }

        public int? Order { get; set; }
    }
}
