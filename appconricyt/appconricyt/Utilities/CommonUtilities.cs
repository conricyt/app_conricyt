﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace appconricyt.Core.Utilities
{
    public static class CommonUtilities
    {
        public static string GetIPAddress()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();

                return externalIP;
            }
            catch { return null; }
        }

        public static string GetSummonDigest(string apiKey, string IDString)
        {
            byte[] key = Encoding.UTF8.GetBytes(apiKey);
            byte[] data = Encoding.UTF8.GetBytes(IDString);

            HMACSHA1 hmac = new HMACSHA1(key);
            CryptoStream cs = new CryptoStream(Stream.Null, hmac, CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.Close();

            byte[] result = hmac.Hash;
            string digest = Convert.ToBase64String(result);

            return digest;
        }

        public static string GenerateIDString(string queryString, string date)
        {
            string IDString = string.Empty;
            string format = "{0}\n{1}\n{2}\n{3}\n{4}\n";

            IDString = string.Format(format, Constants.AcceptHeaderValue, date, Constants.ApiBaseUrl, Constants.ApiSearchUrl, queryString);

            return IDString;
        }

        public static string CleanString(string data) {
            data = data.Replace("<h>", "");
            data = data.Replace("</h>", "");
            return data;
        }
    }
}
