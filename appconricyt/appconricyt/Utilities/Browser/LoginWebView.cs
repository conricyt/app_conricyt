﻿using appconricyt.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace appconricyt.Browser
{
    public class LoginWebView : ContentPage
    {
        //this needs to be defined at class level for use within methods.
        private WebView webView;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebViewSample.InAppBrowserXaml"/> class.
        /// Takes a URL indicating the starting page for the browser control.
        /// </summary>
        /// <param name="URL">URL to display in the browser.</param>
        public LoginWebView(MainPage mainPage, bool logout = false)
        {
            var layout = new StackLayout();
            //WebView needs to be given a height and width request within layouts to render
            webView = new WebView() { WidthRequest = 1000, HeightRequest = 1000 };            

            var htmlSource = new HtmlWebViewSource();
            string html = @"<html><head><script>function post(){var e=new XMLHttpRequest;e.withCredentials=!0,e.addEventListener(""readystatechange"",function(){4===this.readyState&&console.log(this.responseText)}),e.open(""POST"",""{LoginUrl}""),e.setRequestHeader(""Content-Type"",""application/x-www-form-urlencoded""),e.send(""user={User}&pass={Password}"")}post();</script></head><body><img src=""Loading.gif"" style=""display: block;margin-left: auto;margin-right: auto;padding-top: 15%;position: relative;"" /></body></html>";

            if(logout)
                html = @"<html><head><script>function post(){var t=new XMLHttpRequest;t.withCredentials=!0,t.addEventListener(""readystatechange"",function(){4===this.readyState&&console.log(this.responseText)}),t.open(""GET"",""{LogoutUrl}""),t.send(null);window.history.back();}post();</script></head><body><img src=""Loading.gif"" style=""display: block;margin-left: auto;margin-right: auto;padding-top: 15%;position: relative;"" /></body></html>";

            html = html.Replace("{LoginUrl}", Constants.LoginUrl);
            html = html.Replace("{LogoutUrl}", Constants.LogoutUrl);
            html = html.Replace("{User}", ExecutionParameters.UserName);
            html = html.Replace("{Password}", ExecutionParameters.Password);

            htmlSource.Html = html;

            webView.Source = htmlSource;

            webView.Navigating += webOnNavigating;
            webView.Navigated += webOnEndNavigating;
            
            layout.Children.Add(webView);
            Content = layout;
            Thread.Sleep(4000);
        }

        void webOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            Thread.Sleep(4000);
        }

        void webOnEndNavigating(object sender, WebNavigatedEventArgs e)
        {
            if (webView.CanGoBack)
            {
                webView.GoBack();
            }
            else
            {
                this.Navigation.PopAsync(); // closes the in-app browser view.
            }
        }
    }
}
