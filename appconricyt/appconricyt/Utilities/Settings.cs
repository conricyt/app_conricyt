﻿using System;
using System.Collections.Generic;
using System.Text;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace appconricyt.Utilities
{
    public static class Settings
    {

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;

        private const string UsernameKey = "username_key";
        private static readonly string UsernameDefault = null;

        private const string PasswordKey = "password_key";
        private static readonly string PasswordDefault = null;

        private const string isLoggedKey = "isLogged_key";
        private static readonly bool isLoggedDefault = false;

        private const string SummonApiKeyKey = "summonApiKey_key";
        private static readonly string SummonApiKeyDefault = null;

        private const string SummonApiUserKey = "summonApiUser_key";
        private static readonly string SummonApiUserDefault = null;

        private const string SummonApiInstanceKey = "summonApiInstance_key";
        private static readonly string SummonApiInstanceDefault = null;

    
        #endregion
        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }

        public static string Username
        {
            get
            {
                return AppSettings.GetValueOrDefault(UsernameKey, UsernameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UsernameKey, value);
            }
        }

        public static string Password
        {
            get
            {
                return AppSettings.GetValueOrDefault(PasswordKey, PasswordDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PasswordKey, value);
            }
        }

        public static bool IsLogged
        {
            get
            {
                return AppSettings.GetValueOrDefault(isLoggedKey, isLoggedDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isLoggedKey, value);
            }
        }

        #region Summon Settings
        public static string SummonApiKey
        {
            get
            {
                return AppSettings.GetValueOrDefault(SummonApiKeyKey, SummonApiKeyDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SummonApiKeyKey, value);
            }
        }

        public static string SummonApiUser
        {
            get
            {
                return AppSettings.GetValueOrDefault(SummonApiUserKey, SummonApiUserDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SummonApiUserKey, value);
            }
        }

        public static string SummonApiInstance
        {
            get
            {
                return AppSettings.GetValueOrDefault(SummonApiInstanceKey, SummonApiInstanceDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SummonApiInstanceKey, value);
            }
        }

        #endregion
    }

}
