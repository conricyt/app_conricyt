﻿using appconricyt.Core.Models.PublisherResource;
using System.Collections.Generic;
using System.Linq;

namespace appconricyt.Core.Extensions
{
    public static class CommonExtensions
    {
        public static List<Publisher> FilterPublishersResourcesByLetter(this PublishersResources publisherResource, string letter)
        {
            if (publisherResource.Is_valid)
            {
                return publisherResource.Publishers.Where(r => r.Name.StartsWith(letter)).ToList();
            }

            return null;
        }
    }
}
