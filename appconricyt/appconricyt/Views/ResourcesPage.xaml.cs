﻿using appconricyt.Browser;
using appconricyt.Core.Extensions;
using appconricyt.Core.Models.PublisherResource;
using appconricyt.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace appconricyt.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResourcesPage : ContentPage
	{
        StackLayout MainLayout = new StackLayout {
            //VerticalOptions = LayoutOptions.CenterAndExpand,
        };
        ScrollView ResourceScroll = new ScrollView();
        public Label labelTapped = new Label();        

        StackLayout searchLetters1 = new StackLayout
        {
            Orientation = StackOrientation.Horizontal,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
        };
        StackLayout searchLetters2 = new StackLayout
        {
            Orientation = StackOrientation.Horizontal,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
        };


        public ResourcesPage ()
		{
			InitializeComponent ();

            MainLayout.Children.Add(new Label
            {
                Text = "Recursos del Consorcio",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize= 16,
            });

            PaintSearchLetter("A");
            PaintResources("A");

            this.Content = MainLayout;

        }

        void PaintSearchLetter(string Letter) {
            /****************letter search*****************/
            
            MainLayout.Children.Remove(searchLetters1);
            MainLayout.Children.Remove(searchLetters2);

            var letters1 = new[]
            {
                new { name = "A" },
                new { name = "B" },
                new { name = "C" },
                new { name = "D" },
                new { name = "E" },
                new { name = "F" },
                new { name = "G" },
                new { name = "H" },
                new { name = "I" },
                new { name = "J" },
                new { name = "K" },
                new { name = "L" },
                new { name = "M" },
            };

            var letters2 = new[]
           {
                new { name = "N" },
                new { name = "O" },
                new { name = "P" },
                new { name = "Q" },
                new { name = "R" },
                new { name = "S" },
                new { name = "T" },
                new { name = "V" },
                new { name = "W" },
                new { name = "X" },
                new { name = "Y" },
                new { name = "Z" },
            };

            var labelTapRecognizer = new TapGestureRecognizer();

            labelTapRecognizer.Tapped += (sender, e) => {
                labelTapped.TextColor = Color.Orange;
                labelTapped = (Label)sender;
                labelTapped.TextColor = Color.FromHex("192a56");
                PaintResources(labelTapped.Text);
            };
            
            foreach (var letter in letters1)
            {  
                Label letterLabel = new Label
                {
                    Text = letter.name,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                };
                var color = Color.Orange;
                if (letter.name == Letter)
                {
                    color = Color.FromHex("192a56");
                    labelTapped = letterLabel;
                }
                letterLabel.TextColor = color;

                searchLetters1.Children.Add(letterLabel);
                letterLabel.GestureRecognizers.Add(labelTapRecognizer);
            }
            foreach (var letter in letters2)
            {
                Label letterLabel = new Label
                {
                    Text = letter.name,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                };
                var color = Color.Orange;
                if (letter.name == Letter)
                {
                    color = Color.FromHex("192a56");
                    labelTapped = letterLabel;
                }
                letterLabel.TextColor = color;

                searchLetters2.Children.Add(letterLabel);
                letterLabel.GestureRecognizers.Add(labelTapRecognizer);
            }

            MainLayout.Children.Add(searchLetters1);
            MainLayout.Children.Add(searchLetters2);
        }

        void PaintResources(string Letter) {
            /**********Resoursources List*************/
            StackLayout ResourceLayout = new StackLayout();
            MainLayout.Children.Remove(ResourceScroll);
            if(ResourcesData.publishersResources == null || ResourcesData.publishersResources.Publishers == null || !ResourcesData.publishersResources.Publishers.Any())
            {
                try
                {
                    ResourcesData.publishersResources = Task.Run(async () => await Xamarin.Forms.DependencyService.Get<IApiConnection>().GetPublishersResources()).Result;
                }
                catch (Exception)
                {
                    ResourcesData.publishersResources = new PublishersResources();
                }
            }

            try
            {
                var publishers = new List<Publisher>();
                publishers = ResourcesData.publishersResources.FilterPublishersResourcesByLetter(Letter);
                if (publishers != null)
                {
                    foreach (var publisher in publishers)
                    {
                        var GridContent = new Grid
                        {
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            RowSpacing = 5,
                            RowDefinitions = {
                            new RowDefinition { Height = GridLength.Auto},
                        }
                        };

                        GridContent.Children.Add(new Label
                        {
                            Text = publisher.Name,
                            TextColor = Color.Black
                        }, 0, 0);

                        Image PublisherImage = new Image
                        {
                            Aspect = Aspect.AspectFit,
                            Source = publisher.Img,
                            HeightRequest = 70
                        };

                        StackLayout ResourceContent = new StackLayout();

                        ResourceContent.Children.Add(PublisherImage);

                        foreach (var resource in publisher.Resources)
                        {
                            var titleHtml = new HtmlFormattedLabel();
                            var resourcesHtml = new HtmlFormattedLabel();
                            titleHtml.Text = "<h5>" + resource.Name + "</h5>";

                            var urlLink = resource.Url;
                            var linkTapRecognizer = new TapGestureRecognizer();
                            linkTapRecognizer.Tapped += (sender, e) =>
                            {
                                CallWebView(urlLink);
                            };

                            titleHtml.GestureRecognizers.Add(linkTapRecognizer);
                            resourcesHtml.Text = "<div>" + resource.Description + "</div>";
                            ResourceContent.Children.Add(titleHtml);
                            ResourceContent.Children.Add(resourcesHtml);
                        }


                        Frame MyFrame = new Frame
                        {
                            OutlineColor = Color.Accent,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                        };

                        try
                        {
                            MyFrame.Content = ResourceContent;
                            ResourceLayout.Children.Add(MyFrame);
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        ResourceScroll.Content = ResourceLayout;
                        MainLayout.Children.Add(ResourceScroll);
                    }
                }
                else
                {
                    Label resultTitle = new Label
                    {
                        Text = "Su búsqueda no arrojó resultados",
                        HorizontalOptions = LayoutOptions.Center,
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                    };
                    MainLayout.Children.Add(resultTitle);
                    MainLayout.Children.Clear();
                }
            }
            catch (Exception e)
            {
                Label resultTitle = new Label
                {
                    Text = "Ha ocurrido un error en la búsqueda",
                    HorizontalOptions = LayoutOptions.Center,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                };
                MainLayout.Children.Clear();
                MainLayout.Children.Add(resultTitle);
            }
        }    

        private void CallWebView(string url)
        {
            this.Navigation.PushAsync(new InAppWebView(url));
        }
    }
}