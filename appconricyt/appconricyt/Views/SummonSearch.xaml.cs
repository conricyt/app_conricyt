﻿using appconricyt.Browser;
using appconricyt.Core;
using appconricyt.Core.Utilities;
using appconricyt.Services;
using appconricyt.Utilities;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appconricyt.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SummonSearch : ContentPage
	{
        
        ScrollView SearchContentScroll = new ScrollView();
        Image FondoConricyt = new Image { Source = "fondoConricyt.png"};
        String searchText = "";


        public SummonSearch()
		{
            InitializeComponent ();
            SearchSummonBox.SearchButtonPressed += OnSearch;
            ResultContent.Children.Add(FondoConricyt);

            this.BindingContext = Device.RuntimePlatform == Device.Android ? SearchSummonBox.TextColor = Color.White : SearchSummonBox.TextColor = Color.Gray;
            AdvancedSearch.IsVisible = false;

            var ShowAdvancedRecognizer = new TapGestureRecognizer();
            ShowAdvancedRecognizer.Tapped += (sender, e) => {
                ShowAdvancedSearch = (Label)sender;
                ShowAdvancedSearchOptions();
            };
            ShowAdvancedSearch.GestureRecognizers.Add(ShowAdvancedRecognizer);

            filterAuthor.CheckChanged += filterAuthorChanged;
            filterAuthor.Checked = ExecutionParameters.FilterAuthor;
            filterFullText.CheckChanged += filterFullTextChanged;
            filterFullText.Checked = ExecutionParameters.FilterFullText;
            filterBook.CheckChanged += filterBookChanged;
            filterBook.Checked = ExecutionParameters.FilterBook;
            filterJournalArticle.CheckChanged += filterJournalArticleChanged;
            filterJournalArticle.Checked = ExecutionParameters.FilterJournalArticle;

        }

        private void ShowAdvancedSearchOptions() {
            if (AdvancedSearch.IsVisible) { AdvancedSearch.IsVisible = false; ShowAdvancedSearch.Text = "Mostrar opciones búsqueda avanzada"; }
            else { AdvancedSearch.IsVisible = true; ShowAdvancedSearch.Text = "Ocultar opciones búsqueda avanzada"; }
        }

        private void filterAuthorChanged(object sender, EventArgs e)
        {
            if (ExecutionParameters.FilterAuthor) { ExecutionParameters.FilterAuthor = false;}
            else { ExecutionParameters.FilterAuthor = true; }
        }

        private void filterFullTextChanged(object sender, EventArgs e)
        {
            if (ExecutionParameters.FilterFullText) {ExecutionParameters.FilterFullText = false;}
            else { ExecutionParameters.FilterFullText = true; }
        }

        private void filterBookChanged(object sender, EventArgs e)
        {
            if (ExecutionParameters.FilterBook) { ExecutionParameters.FilterBook = false; }
            else { ExecutionParameters.FilterBook = true; }
        }

        private void filterJournalArticleChanged(object sender, EventArgs e)
        {
            if (ExecutionParameters.FilterJournalArticle) { ExecutionParameters.FilterJournalArticle = false; }
            else { ExecutionParameters.FilterJournalArticle = true; }
        }

        private void PaintPaginator(string searchText, int pageNumber, int totalPages) {
            StackLayout PaginatorLabels = new StackLayout
            {
                HeightRequest = 50,
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            Label pageLabel = new Label {
                Text = pageNumber.ToString(), TextColor = Color.White
            };
            Label totalPagesLabel = new Label {
                Text = "de "+totalPages, TextColor = Color.White
            };

            var previous = new Label() {
                Text = pageNumber == 1 ? "" :  "◄ Prev     ",
                TextColor = Color.Orange };

            var next = new Label() {
                Text = pageNumber == totalPages ? "" : "     Next ►",
                TextColor = Color.Orange
            };

            var prevTapRecognizer = new TapGestureRecognizer();
            prevTapRecognizer.Tapped += (sender, e) => {
                previous = (Label)sender;
                SearchByPage(searchText, pageNumber-1);
            };
            previous.GestureRecognizers.Add(prevTapRecognizer);


            var nextTapRecognizer = new TapGestureRecognizer();
            nextTapRecognizer.Tapped += (sender, e) => {
                previous = (Label)sender;
                SearchByPage(searchText, pageNumber + 1);
            };
            next.GestureRecognizers.Add(nextTapRecognizer);

            PaginatorLabels.Children.Clear();
            PaginatorLabels.Children.Add(previous);
            PaginatorLabels.Children.Add(pageLabel);
            PaginatorLabels.Children.Add(totalPagesLabel);
            PaginatorLabels.Children.Add(next);
            PaginatorContent.Children.Clear();
            PaginatorContent.Children.Add(PaginatorLabels);
        }

        private void SearchByPage(string searchText, int pageNumber = 1) {
            StackLayout MainLayout = new StackLayout();
            MainLayout.Children.Remove(SearchContentScroll);

            try
            {
                var searchResult = Task.Run(async () => await Xamarin.Forms.DependencyService.Get<IApiConnection>().Search(
                    searchText, 
                    pageNumber,
                    20, //pageSize
                    ExecutionParameters.FilterFullText,
                    ExecutionParameters.FilterBook,
                    ExecutionParameters.FilterJournalArticle,
                    ExecutionParameters.FilterAuthor
                    )).Result;
                ResultContent.Children.Remove(FondoConricyt);
                if (searchResult != null)
                {
                    var totalPages = searchResult.PageCount / searchResult.Query.PageSize;
                    PaintPaginator(searchText, pageNumber, totalPages);

                    var pageOffset = (searchResult.Query.PageSize) * ((pageNumber - 1));

                    for (var i = 0; i < searchResult.Documents.Count; i++)
                    {
                        var Document = searchResult.Documents;
                        StackLayout GridContent = new StackLayout
                        {
                            VerticalOptions = LayoutOptions.FillAndExpand,
                        };

                        var title = (pageOffset + i + 1) + ". " + CommonUtilities.CleanString(Document[i].Title[0]);

                        if (Document[i].Subtitle != null && Document[i].Subtitle.Count > 0)
                            title += ": " + Document[i].Subtitle[0];

                        Label titleLabel = new Label
                        {
                            Text = title,
                            FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                            TextColor = Color.FromHex("192a56"),
                        };
                        var urlLink = GetUrlWithProxy(Document[i].Link);
                        
                        var linkTapRecognizer = new TapGestureRecognizer();
                        linkTapRecognizer.Tapped += (sender, e) => {
                            titleLabel = (Label)sender;
                            CallWebView(urlLink);
                        };

                        titleLabel.GestureRecognizers.Add(linkTapRecognizer);
                        GridContent.Children.Add(titleLabel);

                        var author_result = "No disponible";
                        if (Document[i].Author != null)
                        {
                            author_result = Document[i].Author[0];
                        }

                        GridContent.Children.Add(new HtmlFormattedLabel
                        {
                            Text = "<strong>Por: </strong> " + author_result,
                            FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label))
                        });

                        var snippet_result = "No disponible";
                        if (Document[i].Snippet != null)
                        {
                            snippet_result = Document[i].Snippet[0];
                        }

                        GridContent.Children.Add(new HtmlFormattedLabel
                        {
                            Text = snippet_result,
                            FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label))
                        });

                        var isbn_result = "No disponible";
                        if (Document[i].ISBN != null)
                        {
                            isbn_result = Document[i].ISBN[0];
                        }

                        GridContent.Children.Add(new HtmlFormattedLabel
                        {
                            Text = "<strong>ISBN: </strong> " + isbn_result,
                            FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label))
                        });

                        var abstract_result = "No disponible";
                        var maxLength = abstract_result.Length;
                        if (Document[i].Abstract != null)
                        {
                            abstract_result = Document[i].Abstract[0];
                            maxLength = abstract_result.Length > 40 ? 40 : abstract_result.Length;
                        }

                        GridContent.Children.Add(new HtmlFormattedLabel
                        {
                            Text = "<strong>Resumen: </strong>" + abstract_result.Substring(0, maxLength),
                            FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label))
                        });

                        Frame FrameResult = new Frame
                        {
                            OutlineColor = Color.Accent,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                        };

                        FrameResult.Content = GridContent;
                        MainLayout.Children.Add(FrameResult);
                    };
                }
                else
                {
                    Label resultTitle = new Label
                    {
                        Text = "Su búsqueda no arrojó resultados",
                        HorizontalOptions = LayoutOptions.Center,
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                    };
                    MainLayout.Children.Add(resultTitle);
                }
            }
            catch(Exception e)
            {
                Label resultTitle = new Label
                {
                    Text = "Ha ocurrido un error en la búsqueda",
                    HorizontalOptions = LayoutOptions.Center,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                };
                MainLayout.Children.Add(resultTitle);
            }
            
            SearchContentScroll.Content = MainLayout;
            ResultContent.Children.Add(SearchContentScroll);
        }

        private string GetUrlWithProxy(string url)
        {
            var uri = new Uri(url);

            if (Settings.IsLogged)
                return url.Replace(uri.Scheme + "://" + uri.Host, Constants.LoginWebUrl + "?user=" + ExecutionParameters.UserName + "&pass=" + ExecutionParameters.Password + "&redirect=" + uri.Scheme + "://" + uri.Host + "." + Constants.ProxyBaseUrl);

            return url;
        }

        private void CallWebView(string url) {
            this.Navigation.PushAsync(new InAppWebView(url));
        }

        private void OnSearch(object sender, EventArgs e)
        {
            searchText = ((SearchBar)sender).Text;
            SearchByPage(searchText, 1);
        }
    };
}
