﻿using appconricyt.Core;
using appconricyt.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appconricyt.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopLoginForm : Rg.Plugins.Popup.Pages.PopupPage
    {
        public MainPage mainPage;

        public PopLoginForm(MainPage mainPage)
        {
            this.mainPage = mainPage;
            InitializeComponent();
        }
        private async void CancelButton_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            var Username = UsernameEntry.Text;
            var Password = PasswordEntry.Text;
            var KeepMeLoggedIn = KeepMeLoggedInEntry.Checked;


            try
            {
                var accessValidation = Task.Run(async () => await Xamarin.Forms.DependencyService.Get<IApiConnection>().ValidateUser(Username, Password)).Result;

                if (accessValidation == null || !accessValidation.Is_valid)
                {
                    ErrorMessage.IsVisible = true;
                }
                else
                {
                    bool wasAuthSuccessful = Task.Run(async () => await Xamarin.Forms.DependencyService.Get<IApiConnection>().LoginUser(Username, Password, KeepMeLoggedIn)).Result;

                    if (!wasAuthSuccessful)
                    {
                        ErrorMessage.IsVisible = true;
                    }
                    else
                    {
                        var loginLogo = mainPage.ToolbarItems.FirstOrDefault(r => r.ClassId == "loginLogo");

                        if (loginLogo != null)
                        {
                            loginLogo.Icon = "logout.png";
                            loginLogo.Command = new Command(this.mainPage.Logout);
                        }

                        ExecutionParameters.SummonApiKey = accessValidation.Api.Secret_key;
                        ExecutionParameters.SummonApiUser = accessValidation.Api.Access_id;
                        ExecutionParameters.SummonApiInstance = accessValidation.Api.Shortname;

                        await PopupNavigation.Instance.PopAsync();
                    }
                }
            }
            catch (Exception)
            {
                ErrorMessage.IsVisible = true;
                ErrorMessage.Text = "No es posible procesar su solicitud en este momento";
            }           
        }
    }
}
