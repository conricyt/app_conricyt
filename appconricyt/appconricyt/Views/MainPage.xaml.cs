﻿using appconricyt.Browser;
using appconricyt.Core;
using appconricyt.Core.Utilities;
using appconricyt.MenuItems;
using appconricyt.Services;
using appconricyt.Utilities;
using appconricyt.Views;
using appconricyt.Views.Forms;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace appconricyt
{
    public partial class MainPage : MasterDetailPage
    {
        public List<MasterPageItem> menuList { get; set; }
        public ToolbarItem LoginItem;

        public MainPage()
        {
            InitializeComponent();
            
            Detail = (Page)Activator.CreateInstance(typeof(SummonSearch));

            Detail.Icon = "icon.png";

            LoginItem = new ToolbarItem();
            var ip = CommonUtilities.GetIPAddress();

            try
            {
                var IpIsValid = Task.Run(async () => await Xamarin.Forms.DependencyService.Get<IApiConnection>().ValidateIpAddress(ip)).Result;

                if (IpIsValid.Is_valid || ExecutionParameters.IsLogged)
                {

                    if (IpIsValid.Is_valid)
                    {
                        LoginItem.Icon = "login.png";
                        LoginItem.Command = new Command(this.ShowLoginForm);

                        ExecutionParameters.SummonApiKey = IpIsValid.Api.Secret_key;
                        ExecutionParameters.SummonApiUser = IpIsValid.Api.Access_id;
                        ExecutionParameters.SummonApiInstance = IpIsValid.Api.Shortname;
                    }
                    else {
                        LoginItem.Icon = "logout.png";
                        LoginItem.Command = new Command(this.Logout);
                    }
                }
                else
                {
                    LoginItem.Icon = "login.png";
                    LoginItem.Command = new Command(this.ShowLoginForm);
                    LoginItem.ClassId = "loginLogo";
                    GetDefaultInstance();
                }

                if (!IpIsValid.Is_valid || !ExecutionParameters.IsLogged)
                {
                    LoginItem.Icon = "login.png";
                    LoginItem.Command = new Command(this.ShowLoginForm);
                    LoginItem.ClassId = "loginLogo";
                    GetDefaultInstance();
                }
                else
                {
                    LoginItem.Icon = "logout.png";
                    LoginItem.Command = new Command(this.Logout);

                    if(IpIsValid.Is_valid)
                    {
                        ExecutionParameters.SummonApiKey = IpIsValid.Api.Secret_key;
                        ExecutionParameters.SummonApiUser = IpIsValid.Api.Access_id;
                        ExecutionParameters.SummonApiInstance = IpIsValid.Api.Shortname;
                    }                    
                }

                this.ToolbarItems.Add(LoginItem);
            }
            catch (Exception)
            {
                LoginItem.Icon = "login.png";
                LoginItem.Command = new Command(this.ShowLoginForm);
                LoginItem.ClassId = "loginLogo";

                this.ToolbarItems.Add(LoginItem);

                GetDefaultInstance();
            }

            menuList = new List<MasterPageItem>();
            var page = new MasterPageItem() { Title = "Inicio", Icon = "ic_launcher.png", TargetType = typeof(SummonSearch) };
            var resources_page = new MasterPageItem() { Title = "Recursos Del Consorcio", Icon = "recursos.png", TargetType = typeof(ResourcesPage) };

            menuList.Add(page);
            menuList.Add(resources_page);

            this.navigationDrawerList.ItemsSource = menuList;

            this.BindingContext = Device.RuntimePlatform == Device.Android ? new { Header = "", Image = "imageHeader.png", } : new { Header = "", Image = "imageHeaderiOS.png", };
        }

        protected override void OnAppearing()
        {
            // Handle when your app starts                      
        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = (Page)Activator.CreateInstance(page);
            IsPresented = false;
        }

        protected void ShowLoginForm()
        {
            PopupNavigation.Instance.PushAsync(new PopLoginForm(this));
        }

        public async void Logout()
        {
            var rta = await DisplayAlert("Confirmación", "¿Está seguro de cerrar sesión?", "Si", "No");
            if (!rta) return;

            Settings.Username = null;
            Settings.Password = null;
            Settings.IsLogged = false;
            ExecutionParameters.KeepMeLoggedIn = false;

            ExecutionParameters.UserName = null;
            ExecutionParameters.Password = null;
            ExecutionParameters.IsLogged = false;

            await this.Navigation.PushAsync(new LoginWebView(this, true));

            LoginItem.Icon = "login.png";
            LoginItem.Command = new Command(this.ShowLoginForm);
            LoginItem.ClassId = "loginLogo";
        }

        private void GetDefaultInstance()
        {
            var defaultSummon = Task.Run(async () => await Xamarin.Forms.DependencyService.Get<IApiConnection>().GetDefaultInstance()).Result;

            if (defaultSummon.Is_valid)
            {
                ExecutionParameters.SummonApiKey = defaultSummon.Api.Secret_key;
                ExecutionParameters.SummonApiUser = defaultSummon.Api.Access_id;
                ExecutionParameters.SummonApiInstance = defaultSummon.Api.Shortname;
            }
        }
    }
}
