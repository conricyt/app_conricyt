﻿using appconricyt.Models.Summon;

namespace appconricyt
{
    public static class SummonResultData
    {
        private static SummonResult _summonResult;

        public static SummonResult summonResult
        {
            get
            {
                if (_summonResult != null)
                    return _summonResult;
                else
                    return null;
            }
            set
            {
                _summonResult = value;
            }
        }
    }
}
