﻿namespace appconricyt.Core
{
    public static class Constants
    {
        #region Urls

        public const string AuthTokenUrl = "http://api.conricyt.mx/v1/oauth/token";
        public const string PublisherResourcesUrl = "http://api.conricyt.mx/v1/publishers_resources";
        public const string ValidateIPAddressUrl = "http://api.conricyt.mx/v1/validate_ipaddress";
        public const string ValidateUserUrl = "http://api.conricyt.mx/v1/validate_user";
        public const string DefaultSummon = "http://api.conricyt.mx/v1/default_summon";
        public const string LoginUrl = "https://conricyt.remotexs.co/user/login";
        public const string LoginWebUrl = "http://www.conricyt.mx/app_auth.php";
        public const string LogoutUrl = "https://conricyt.remotexs.co/user/logout";
        public const string ProxyBaseUrl = "conricyt.remotexs.co";

        #endregion

        #region Auth Parameters

        public const string ClientSecret = "a3ec17cb2e95194fbf18aa1bcd5cad28";
        public const string ClientId = "etech";
        public const string GrantType = "client_credentials";

        #endregion

        #region Summon Parameters

        public const string AcceptHeaderValue = "application/json";
        public const string ApiBaseUrl = "api.summon.serialssolutions.com";
        public const string ApiSearchUrl = "/2.0.0/search";
        public const string SummonProtocol = "http";

        #endregion
    }
}
