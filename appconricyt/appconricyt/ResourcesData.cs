﻿
using appconricyt.Core.Models.PublisherResource;

namespace appconricyt
{
    public static class ResourcesData
    {
        private static PublishersResources _publishersResources;

        public static PublishersResources publishersResources{ 
            get
            {
                if (_publishersResources != null)
                    return _publishersResources;
                else
                    return null;
            }
            set
            {
                _publishersResources = value;
            }
        }
    }
}
