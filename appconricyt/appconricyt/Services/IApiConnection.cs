﻿using appconricyt.Core.Models.AccessValidation;
using appconricyt.Core.Models.PublisherResource;
using appconricyt.Models.AccessValidation;
using appconricyt.Models.Summon;
using System.Threading.Tasks;

namespace appconricyt.Services
{
    public interface IApiConnection
    {
        Task<string> AuthorizeApp();
        Task<PublishersResources> GetPublishersResources();
        Task<User> ValidateUser(string user, string password);
        Task<IPAddress> ValidateIpAddress(string ip);
        Task<bool> LoginUser(string user, string password, bool keepMeLoggedIn);
        Task<DefaultSummon> GetDefaultInstance();
        Task<SummonResult> Search(string param, int pageNumber = 1, int pageSize = 20, bool filterFullText = false, bool filterBook = false, bool filterJournalArticle = false, bool filterByAuthor = false);
    }
}