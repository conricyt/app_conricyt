﻿using appconricyt.Core;
using System.Threading;
using Xamarin.Forms;

namespace appconricyt.Browser
{
    public class LoginWebView : ContentPage
    {
        //this needs to be defined at class level for use within methods.
        private WebView webView;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebViewSample.InAppBrowserXaml"/> class.
        /// Takes a URL indicating the starting page for the browser control.
        /// </summary>
        /// <param name="URL">URL to display in the browser.</param>
        public LoginWebView(MainPage mainPage, bool logout = false)
        {
            var layout = new StackLayout();
            //WebView needs to be given a height and width request within layouts to render
            webView = new WebView() { WidthRequest = 1000, HeightRequest = 1000 };

            webView.Navigated += webOnEndNavigating;

            var htmlSource = new HtmlWebViewSource();
            string html = "";

            if (logout)
            {
                html = @"<html><head><script>function post(){var t=new XMLHttpRequest;t.withCredentials=!0,t.addEventListener(""readystatechange"",function(){4===this.readyState&&console.log(this.responseText)}),t.open(""GET"",""{LogoutUrl}""),t.send(null);setTimeout(function(){ window.location.href = ""#""; }, 3000);}post();</script></head><body><img src=""Loading.gif"" style=""display: block;margin-left: auto;margin-right: auto;padding-top: 15%;position: relative;"" /></body></html>";
                html = html.Replace("{LoginUrl}", Constants.LoginWebUrl);
                html = html.Replace("{LogoutUrl}", Constants.LogoutUrl);
                html = html.Replace("{User}", ExecutionParameters.UserName);
                html = html.Replace("{Password}", ExecutionParameters.Password);
                htmlSource.Html = html;
                webView.Source = htmlSource;
            }

            layout.Children.Add(webView);
            Content = layout;
        }

        void webOnEndNavigating(object sender, WebNavigatedEventArgs e)
        {
            Thread.Sleep(2000);
            if (webView.CanGoBack)
            {                
                webView.GoBack();
            }
            else
            {
                this.Navigation.PopAsync(); // closes the in-app browser view.
            }
        }
    }
}
