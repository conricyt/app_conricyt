﻿using System;
using Xamarin.Forms;

namespace appconricyt.Browser
{
    public class InAppWebView : ContentPage
    {
        //this needs to be defined at class level for use within methods.
        private WebView webView;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebViewSample.InAppBrowserXaml"/> class.
        /// Takes a URL indicating the starting page for the browser control.
        /// </summary>
        /// <param name="URL">URL to display in the browser.</param>
        public InAppWebView(string URL)
        {
            this.Title = URL;
            var layout = new StackLayout();

            //WebView needs to be given a height and width request within layouts to render
            webView = new WebView() { WidthRequest = 1000, HeightRequest = 1000, Source = URL };
            layout.Children.Add(webView);

            Content = layout;
        }
    }
}
