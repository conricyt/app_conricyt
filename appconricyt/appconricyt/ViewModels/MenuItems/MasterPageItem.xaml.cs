﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace appconricyt.MenuItems
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPageItem : ContentPage
	{
        //public string Title { get; set; }
        //public string Icon { get; set; }
        public Type TargetType { get; set; }
	}
}