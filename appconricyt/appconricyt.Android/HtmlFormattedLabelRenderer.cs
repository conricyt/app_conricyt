﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using appconricyt;
using appconricyt.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(HtmlFormattedLabel), typeof(HtmlFormattedLabelRenderer))]
namespace appconricyt.Droid
{
    public class HtmlFormattedLabelRenderer : LabelRenderer
    {

        public HtmlFormattedLabelRenderer(Context context) : base(context) => AutoPackage = false;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);

            var view = (HtmlFormattedLabel)Element;
            if (view == null) return;

            var text = view.Text.ToString();
            text = text.Replace("<li>", "<p>• ");
            text = text.Replace("</li>", "</p>");
            text = text.Replace("\n", "");
            text = text.Replace("\t", "");
            text = text.Replace("/n", "");
            text = text.Replace("/t", "");
            text = text.Replace("\r", "");
            text = text.Replace("/r", "");
            Control.SetText(Html.FromHtml(text), TextView.BufferType.Spannable);
        }
    }
}