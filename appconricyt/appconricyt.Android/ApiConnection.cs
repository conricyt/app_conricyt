﻿using appconricyt.Core;
using appconricyt.Core.Models.AccessValidation;
using appconricyt.Core.Models.Authorization;
using appconricyt.Core.Models.PublisherResource;
using appconricyt.Core.Utilities;
using appconricyt.Droid;
using appconricyt.Models.AccessValidation;
using appconricyt.Models.Summon;
using appconricyt.Services;
using appconricyt.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ApiConnection))]
namespace appconricyt.Droid
{
    public class ApiConnection : IApiConnection
    {
        public async Task<string> AuthorizeApp()
        {
            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("client_secret", Constants.ClientSecret),
                    new KeyValuePair<string, string>("grant_type", Constants.GrantType),
                    new KeyValuePair<string, string>("client_id", Constants.ClientId)
                });

            var result = await ExecuteRequest(Constants.AuthTokenUrl, content, true);

            if (!string.IsNullOrEmpty(result))
            {
                var objt = JsonConvert.DeserializeObject<AuthToken>(result);

                if (!string.IsNullOrEmpty(objt.Access_token))
                    ExecutionParameters.AuthToken = objt.Access_token;
            }

            return ExecutionParameters.AuthToken;
        }

        public async Task<PublishersResources> GetPublishersResources()
        {
            var result = await ExecuteRequest(Constants.PublisherResourcesUrl);

            if (!string.IsNullOrEmpty(result))
                return JsonConvert.DeserializeObject<PublishersResources>(result);

            return null;
        }

        public async Task<User> ValidateUser(string user, string password)
        {
            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("user", user),
                    new KeyValuePair<string, string>("password", password)
                });

            var result = await ExecuteRequest(Constants.ValidateUserUrl, content);

            if (!string.IsNullOrEmpty(result))
                return JsonConvert.DeserializeObject<User>(result);

            return null;
        }

        public async Task<Core.Models.AccessValidation.IPAddress> ValidateIpAddress(string ip)
        {
            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("ip_address", ip)
                });

            var result = await ExecuteRequest(Constants.ValidateUserUrl, content);

            if (!string.IsNullOrEmpty(result))
                return JsonConvert.DeserializeObject<Core.Models.AccessValidation.IPAddress>(result);

            return null;
        }

        public async Task<bool> LoginUser(string user, string password, bool keepMeLoggedIn)
        {
            bool isLogged = false;

            var content = new FormUrlEncodedContent(
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("user", user),
                    new KeyValuePair<string, string>("pass", password)
                });

            var httpClient = new HttpClient();
            var httpContent = new HttpRequestMessage(HttpMethod.Post, Constants.LoginUrl);

            httpContent.Headers.ExpectContinue = false;

            if (content != null)
                httpContent.Content = content;

            HttpResponseMessage response = await httpClient.SendAsync(httpContent);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                isLogged = response.RequestMessage.RequestUri.AbsoluteUri != Constants.LoginUrl;

            if(isLogged)
            {
                if (keepMeLoggedIn) {
                    ExecutionParameters.KeepMeLoggedIn = keepMeLoggedIn;
                }
                ExecutionParameters.UserName = user;
                ExecutionParameters.Password = password;
                ExecutionParameters.IsLogged = true;
                //variable keepmeloggedin
            }

            return isLogged;
        }

        public async Task<SummonResult> Search(string param, int pageNumber = 1, int pageSize = 20, bool filterFullText = false, bool filterBook = false, bool filterJournalArticle = false, bool filterByAuthor = false)
        {
            #region Build query string

            string facetValueFilter = string.Empty;
            string queryString = "";

            if (filterByAuthor)
                queryString += "s.fq=Author:(" + param + ")";

            if (!string.IsNullOrEmpty(queryString))
                queryString += "&";

            if (filterBook)
                facetValueFilter = string.IsNullOrEmpty(facetValueFilter) ? "s.fvf=ContentType,Book / eBook,false" : facetValueFilter + "&s.fvf=ContentType,Book / eBook,false";

            if (filterJournalArticle)
                facetValueFilter = string.IsNullOrEmpty(facetValueFilter) ? "s.fvf=ContentType,Journal Article,false" : facetValueFilter + "&s.fvf=ContentType,Journal Article,false";

            if (filterFullText)
                facetValueFilter = string.IsNullOrEmpty(facetValueFilter) ? "s.fvf=IsFullText,true,false" : facetValueFilter + "&s.fvf=IsFullText,true,false";

            if (!string.IsNullOrEmpty(facetValueFilter))
                queryString += facetValueFilter;

            if (!string.IsNullOrEmpty(facetValueFilter))
                queryString += "&";

            queryString += "s.ho=true&s.l=es-ES&";

            queryString += "s.pn=" + pageNumber + "&s.ps=" + pageSize;

            if (!filterByAuthor)
                queryString += "&s.q=" + param;

            #endregion

            SummonResult summonResult = null;

            var result = await ExecuteRequestSummon(queryString);
            if (!string.IsNullOrEmpty(result))
                summonResult = JsonConvert.DeserializeObject<SummonResult>(result);

            return summonResult;
        }

        protected static async Task<string> ExecuteRequestSummon(string queryString)
        {
            try
            {
                string date = DateTime.UtcNow.ToString("ddd, dd MMM yyyy HH:mm:ss", CultureInfo.CreateSpecificCulture("en-US")) + " GMT";
                string IDString = CommonUtilities.GenerateIDString(queryString, date);
                string digest = CommonUtilities.GetSummonDigest(ExecutionParameters.SummonApiKey, IDString);

                string requestUrl = Constants.SummonProtocol + "://" + Constants.ApiBaseUrl + Constants.ApiSearchUrl + "?" + queryString;

                string result = "";

                var httpClient = new HttpClient();
                var httpContent = new HttpRequestMessage(HttpMethod.Get, requestUrl);

                httpContent.Headers.ExpectContinue = false;
                httpContent.Headers.Add("Authorization", "Summon " + ExecutionParameters.SummonApiUser + ";" + digest);
                httpContent.Headers.Add("x-summon-date", date);
                httpContent.Headers.Add("Accept", Constants.AcceptHeaderValue);

                HttpResponseMessage response = await httpClient.SendAsync(httpContent);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = await response.Content.ReadAsStringAsync();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected async Task<string> ExecuteRequest(string url, FormUrlEncodedContent content = null, bool isAuthRequest = false)
        {
            var httpClient = new HttpClient();
            var httpContent = new HttpRequestMessage(HttpMethod.Post, url);

            httpContent.Headers.ExpectContinue = false;

            if (!isAuthRequest)
            {
                if (string.IsNullOrEmpty(ExecutionParameters.AuthToken))
                    await AuthorizeApp();

                httpContent.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", ExecutionParameters.AuthToken);
            }

            if (content != null)
                httpContent.Content = content;

            HttpResponseMessage response = await httpClient.SendAsync(httpContent);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                await AuthorizeApp();
                return await ExecuteRequest(url, content, isAuthRequest);
            }

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return await response.Content.ReadAsStringAsync();

            return null;
        }

        public async Task<DefaultSummon> GetDefaultInstance()
        {
            var result = await ExecuteRequest(Constants.DefaultSummon);

            if (!string.IsNullOrEmpty(result))
                return JsonConvert.DeserializeObject<DefaultSummon>(result);

            return null;
        }
    }
}
