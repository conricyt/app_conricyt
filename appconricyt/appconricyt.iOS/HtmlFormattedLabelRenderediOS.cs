﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
using appconricyt.iOS;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using appconricyt;

[assembly: ExportRenderer(typeof(HtmlFormattedLabel), typeof(HtmlFormattedLabelRenderediOS))]
namespace appconricyt.iOS
{
    public class HtmlFormattedLabelRenderediOS : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);

            var view = (HtmlFormattedLabel)Element;
            if (view == null) return;

            var attr = new Foundation.NSAttributedStringDocumentAttributes();
            var nsError = new NSError();
            attr.DocumentType = NSDocumentType.HTML;
            var text = view.Text.ToString();
            text = text.Replace("<li>", "<p>- ");
            text = text.Replace("</li>", "</p>");
            text = text.Replace("<ul>", "");
            text = text.Replace("</ul>", "");
            text = text.Replace("\n", "");
            text = text.Replace("\r", "");
            text = text.Replace("/r", "");
            text = text.Replace("/n", "");
            text = text.Replace("\t", "");
            text = text.Replace("ñ", "n");
            text = text.Replace("Ñ", "N");
            text = GetNewText(text);
            Control.AttributedText = new NSAttributedString(text, attr, ref nsError);
        }

        /***changes accents***/
        string GetNewText(string Text)
        {
            var text_text = Text;
            string newText = "";

            if (Text != null && Text != "")
            {
                Dictionary<string, string> replacementText = new Dictionary<string, string>();
                replacementText.Add("á", "a");
                replacementText.Add("é", "e");
                replacementText.Add("í", "i");
                replacementText.Add("ó", "o");
                replacementText.Add("ú", "u");
                replacementText.Add("Á", "A");
                replacementText.Add("É", "E");
                replacementText.Add("Í", "I");
                replacementText.Add("Ó", "O");
                replacementText.Add("Ú", "U");

                for (int i = 0; i < Text.Length; i++)
                {
                    if (replacementText.ContainsKey(Text.Substring(i, 1)))
                    {
                        var vocal = Text.Substring(i, 1);
                        var vocal2 = replacementText[Text.Substring(i, 1)];
                        newText += replacementText[Text.Substring(i, 1)];
                    }
                    else
                    {
                        newText += Text.Substring(i, 1);
                    }
                }
            }

            return newText;
        }
        /***changes accents***/
    }
}