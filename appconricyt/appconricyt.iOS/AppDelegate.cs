﻿using System;
using System.Collections.Generic;
using System.Linq;
using AsNum.XFControls.iOS;
using Foundation;
using UIKit;

namespace appconricyt.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Rg.Plugins.Popup.Popup.Init();

            AsNumAssemblyHelper.HoldAssembly();
            global::Xamarin.Forms.Forms.Init();

            UINavigationBar.Appearance.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
            UINavigationBar.Appearance.BarTintColor = UIColor.FromRGBA(25, 42, 86, 1);
            UINavigationBar.Appearance.TintColor = UIColor.FromRGBA(255, 255, 255, 1);
            UINavigationBar.Appearance.TintColor = UIColor.White;
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
            {
                TextColor = UIColor.White
            });

            LoadApplication(new App());
            return base.FinishedLaunching(app, options);
        }
    }
}
